ActiveAdmin.register Project do
  remove_filter :manager_projects, :tasks, :invoices

  permit_params :params, :title, :discription, :hour_cost, employee_projects_attributes: [:id, :user_id, :_destroy], manager_projects_attributes: [:id, :manager_id, :_destroy]

  form do |f|
    f.inputs do
      f.input :title
      f.input :discription
      f.input :hour_cost

      f.has_many :employee_projects, heading: 'Add employee', allow_destroy: true, new_record: true do |t|
        t.input :user
      end

      f.has_many :manager_projects, heading: 'Add manager', allow_destroy: true, new_record: true do |t|
        t.input :manager
      end
    end
    f.actions
  end

  index do
    selectable_column
    column :id
    column :title
    column  :created_at
    column  :hour_cost
    actions
  end

  show do
    attributes_table do
      rows :title, :discription, :hour_cost, :created_at
    end

    panel "Employee Projects" do
      table_for project.employee_projects do
        column :user
        column :created_at
      end
    end

    panel "Manager Projects" do
      table_for project.manager_projects do
        column :manager
        column :created_at
      end
    end

    active_admin_comments
  end
end
