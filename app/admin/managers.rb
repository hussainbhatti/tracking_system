ActiveAdmin.register Manager do
  remove_filter :manager_projects, :tasks, :expenses, :image_attachment, :image_blob, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :image

  action_item do
    link_to 'Invite New Manager', new_invitation_admin_managers_path
  end

  collection_action :new_invitation do
    @manager = Manager.new
  end

  member_action :reinvite, method: :post do
    resource.invite! { |manager| manager.invitations_count += 1  }
    redirect_to admin_managers_path notice: 'Invitation sent successfully.'
  end

  collection_action :send_invitation, method: :post do
    @manager = Manager.invite!(params[:manager].permit!, current_manager)
    if @manager.errors.empty?
      flash[:success] = "Manager has been successfully invited."
      redirect_to admin_managers_path
    else
      messages = @manager.errors.full_messages.map { |msg| msg }.join
      flash[:error] = "Error: #{ messages }"
      redirect_to new_invitation_admin_managers_path
    end
  end

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :image, as: :file
    end
    f.actions
  end

  index do
    selectable_column
    column :id
    column :first_name
    column :last_name
    column :email
    column :created_at
    column :invitations_count
    column "Invitation status", :invitation_status

    actions defaults: true do |manager|
      if manager.invitation_status == "Pending"
        item 'Resend invitation', reinvite_admin_manager_path(manager), method: :post
      end
    end
  end

  show do
    attributes_table do
      rows :id, :first_name, :last_name, :email, :created_at
    end
    active_admin_comments
  end
end
