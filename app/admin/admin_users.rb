ActiveAdmin.register AdminUser do
  remove_filter :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
