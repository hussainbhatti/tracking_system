ActiveAdmin.register User do
  remove_filter :employee_projects, :tasks, :expenses, :image_attachment, :image_blob, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :image

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :image, as: :file
    end
    f.actions
  end

  index do
    selectable_column
    column :id
    column :first_name
    column :last_name
    column :email
    column :created_at
    actions
  end

  show do
    attributes_table do
      rows :id, :first_name, :last_name, :email, :created_at
    end
    active_admin_comments
  end
end
