class Expense < ApplicationRecord
  scope :filter_on_dates, -> (start_time, end_time) { where("date >= ? and date <= ?", start_time.to_datetime, end_time.to_datetime.end_of_day) }
  has_many :expense_reports
  has_many :reports, through: :expense_reports

  belongs_to :expenseable, polymorphic: true , optional: true

  validates :title, :amount, :expense_type, :date, presence: true

  enum expense_type: {
    salary: 0,
    bill: 1,
    rent: 2,
    maintenence: 3,
    others: 4,
  }
end
