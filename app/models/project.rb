class Project < ApplicationRecord
  has_many :manager_projects, dependent: :destroy
  has_many :managers, through: :manager_projects

  has_many :employee_projects, dependent: :destroy
  has_many :users, through: :employee_projects
  has_many :tasks, dependent: :destroy
  has_many :invoices, dependent: :destroy

  validates :title, presence: true

  accepts_nested_attributes_for :manager_projects
  accepts_nested_attributes_for :employee_projects, reject_if: :all_blank, allow_destroy: true

  def total_hours_for tasks
    tasks.collect(&:total_hours).sum
  end

end
