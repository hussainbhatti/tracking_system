class Task < ApplicationRecord
  scope :bill_able, -> { where(bill_able: true) }
  scope :filter_on, -> (date_range) { joins(:time_slots).where("start_time >= ? and end_time <= ?", date_range.formatted_start_time, date_range.formatted_end_time).uniq }

  has_many :task_reports
  has_many :reports, through: :task_reports

  belongs_to :taskable, polymorphic: true
  belongs_to :project
  has_many :time_slots, dependent: :destroy

  delegate :hour_cost, to: :project, allow_nil: true

  accepts_nested_attributes_for :time_slots, reject_if: :all_blank, allow_destroy: true

  validates_uniqueness_of :title

  def total_hours
    time_slots.collect(&:hours).sum
  end

  def price
    total_hours * hour_cost
  end
end
