class TimeSlot < ApplicationRecord
  belongs_to :task

  def hours
    (end_time - start_time).to_i / 3600
  end
end
