class Manager < ApplicationRecord
  has_many :manager_projects
  has_many :projects, through: :manager_projects
  has_many :tasks, as: :taskable
  has_many :expenses, as: :expenseable
  has_many :invited_managers, -> { where invited_by_type: "Manager" }, class_name: "Manager", foreign_key: "invited_by_id"
  has_one_attached :image
  validate :user_image?

  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :invitable

  delegate :invitation_not_accepted, to: :invited_managers, allow_nil: true, prefix: true
  delegate :invitation_accepted, to: :invited_managers, allow_nil: true, prefix: true

  def full_name
    "#{first_name} #{last_name}"
  end

  def invitation_status
    return "Not applicable" if invitation_accepted_at.nil? && invited_by_id.nil?
    return "Accepted" if invitation_accepted_at?
    "Pending"
  end

  private
    def user_image?
      errors.add(:base, 'Please upload your image.') unless image.attached?
    end
end
