class TaskReport < ApplicationRecord
  belongs_to :report
  belongs_to :task
end
