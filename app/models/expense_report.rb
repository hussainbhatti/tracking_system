class ExpenseReport < ApplicationRecord
  belongs_to :report
  belongs_to :expense
end
