class Report < ApplicationRecord
  has_many :expense_reports, dependent: :destroy
  has_many :expenses, through: :expense_reports

  has_many :task_reports, dependent: :destroy
  has_many :tasks, through: :task_reports

  accepts_nested_attributes_for :expense_reports
  accepts_nested_attributes_for :task_reports, reject_if: :all_blank, allow_destroy: true

  def total_incomes tasks
    tasks.collect(&:price).sum
  end

  def total_expenses expenses
    expenses.collect(&:amount).sum
  end
end
