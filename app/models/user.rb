class User < ApplicationRecord
  has_many :employee_projects
  has_many :projects, through: :employee_projects
  has_many :tasks, as: :taskable
  has_many :expenses, as: :expenseable
  has_one_attached :image
  validate :user_image?

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def full_name
    "#{first_name} #{last_name}"
  end

  private
    def user_image?
      errors.add(:base, 'Please upload your image.') unless image.attached?
    end
end
