class DateRange
  attr_accessor :start_date, :end_date

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def valid_range?
    begin
      return false if missing_date? || formatted_start_time > formatted_end_time
      return true
    rescue
      return false
    end
  end

  def formatted_start_time
    start_date.to_datetime
  end

  def formatted_end_time
    end_date.to_datetime.end_of_day
  end

  private

    def missing_date?
      start_date.blank? || end_date.blank?
    end
end
