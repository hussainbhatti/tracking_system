class User::TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, only: [:show, :edit, :destroy, :update]

  def index
    @tasks = current_user.tasks.where(project_id: @project)
  end

  def new
    @task = Task.new
    @task.time_slots.build
  end

  def show
  end

  def edit
    @task.time_slots.build unless @task.time_slots.present?
  end

  def create
    @task = current_user.tasks.new(task_params.merge(project: @project))
    if @task.save
      redirect_to user_project_tasks_path(@project, @task)
    else
      render :new
    end
  end

  def update
    if @task.update(task_params)
      redirect_to user_project_tasks_path(@project)
    else
      render :edit
    end
  end

  def destroy
    if @task.destroy
      redirect_to user_project_tasks_path(@project)
    else
      redirect_to user_project_tasks_path(@project), alert: 'Something went wrong'
    end
  end

  private
    def task_params
      params.require(:task).permit(:title, :discription, :bill_able, time_slots_attributes: [:id, :start_time, :end_time, :_destroy])
    end

    def set_project
      @project = current_user.projects.find(params[:project_id])
    end

    def set_task
      @task = current_user.tasks.find(params[:id])
    end
end
