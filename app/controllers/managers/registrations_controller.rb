class Managers::RegistrationsController < Devise::RegistrationsController
  protected
    def update_resource(resource, params)
      resource.update_with_password(sign_up_edit_params)
    end
  private
    def sign_up_edit_params
      params.require(:manager).permit(:first_name, :last_name, :password, :password_confirmation, :current_password, :email, :image)
    end
    def sign_up_params
      params.require(:manager).permit(:first_name, :last_name, :email, :password, :password_confirmation, :image)
    end
end
