class Managers::InvitationsController < Devise::InvitationsController
  before_action :configure_permitted_parameters, if: :devise_controller?

  def new
    @pending_invitations = current_manager.invited_managers_invitation_not_accepted
    @accepted_invitations = current_manager.invited_managers_invitation_accepted
    super
  end

  def create
    self.resource = invite_resource
    resource_invited = resource.errors.empty?

    yield resource if block_given?

    flash.now[:notice] = "Invitation was sent successfully" if resource_invited
  end

  def reinvite
    @manager = Manager.find(params[:id])
    @manager.invite! { |manager| manager.invitations_count += 1  }
    flash.now[:notice] = "Invitation was sent successfully"
  end

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:accept_invitation)
    end

    def update_resource_params
      params.require(:manager).permit(:invitation_token, :first_name, :last_name, :password, :password_confirmation, :image)
    end

  private
    def invite_resource
      Manager.invite!({email: params[:manager][:email]}, current_manager)
    end
end
