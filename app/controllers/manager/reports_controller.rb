class Manager::ReportsController < ApplicationController
  before_action :set_report, only: [:show, :update, :destroy]

  def index
    @reports = Report.all
  end

  def new
    date_range = DateRange.new(report_params[:start_time], report_params[:end_time]) if params[:report].present?

    if params[:report].present? && date_range.valid_range?
      @report = Report.new(start_time: report_params[:start_time], end_time: report_params[:end_time])
      @report.tasks = @tasks = Task.bill_able.filter_on date_range
      @report.expenses = @expenses = Expense.filter_on_dates(report_params[:start_time], report_params[:end_time])
    else
      @report = Report.new
    end
  end

  def show
    @tasks = @report.tasks
    @expenses = @report.expenses
  end

  def create
    @report =  Report.new(report_params)
    if @report.save
      redirect_to manager_reports_path, notice: 'Report was created successfully.'
    else
      render :new
    end
  end

  def destroy
    @report.destroy
    redirect_to manager_reports_path alert: 'Report was destroyed successfully.'
  end

  private
    def report_params
      params.require(:report).permit(:total_income, :total_expense, :start_time, :end_time, expense_reports_attributes: [:expense_id], task_reports_attributes: [:task_id])
    end

    def set_report
      @report = Report.find(params[:id])
    end
end
