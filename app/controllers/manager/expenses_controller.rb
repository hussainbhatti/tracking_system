class Manager::ExpensesController < ApplicationController
  before_action :set_expense, only: [:edit, :show, :update, :destroy]
  before_action :set_form_data, only: [:new, :edit, :update, :create]

  def index
    @expenses = Expense.all
  end

  def new
    @expense = Expense.new
  end

  def show
  end

  def edit
  end

  def create
    @expense = Expense.new(expense_params)
    if @expense.save
      redirect_to manager_expenses_path notice: 'Expense was created successfully.'
    else
      render :new
    end
  end

  def update
    if @expense.update(expense_params)
      redirect_to manager_expenses_path, notice: 'Expense was updated successfully.'
    else
      render :edit
    end
  end

  def destroy
    @expense.destroy
    redirect_to manager_expenses_path alert: 'Expense was destroyed successfully.'
  end

  private
    def expense_params
      params.require(:expense).permit(:recurring_expense, :title, :amount, :date, :expenseable_type, :expenseable_id, :expense_type, :manager_id)
    end

    def set_expense
      @expense = Expense.find(params[:id])
    end

    def set_form_data
      @expenseables = (User.all + Manager.all).collect { |expenseable| [expenseable.full_name.concat(" - #{expenseable.class}"), expenseable.id, { data_type: expenseable.class } ] }
      @expense_types = Expense.expense_types.keys
    end

end
