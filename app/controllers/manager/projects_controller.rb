class Manager::ProjectsController < ApplicationController
  before_action :authenticate_manager!
  before_action :set_project, only: [:edit, :show, :update, :destroy]
  before_action :authorize_user, only: [:edit, :update, :destroy]
  before_action :users_list, only: [:new, :edit, :update, :create]

  def index
    @search = current_manager.projects.search(params[:q])
    @projects = @search.result
  end

  def new
    @project = Project.new
    @project.employee_projects.build
  end

  def show
  end

  def edit
    @project.employee_projects.build unless @project.employee_projects.present?
  end

  def create
    @project = Project.new(project_params.merge(manager_projects_attributes: [manager_id: current_manager.id]))

    if @project.save
      redirect_to manager_project_path(@project)
    else
      render :new
    end
  end

  def update
    if @project.update(project_params)
      redirect_to manager_project_path(@project)
    else
      render :edit
    end
  end

  def destroy
    if @project.destroy
      redirect_to manager_projects_path
    else
      redirect_to manager_projects_path, alert: 'Something went wrong'
    end
  end

  private
    def project_params
      params.require(:project).permit(:title, :discription, :hour_cost, employee_projects_attributes: [:id, :user_id, :_destroy])
    end

    def set_project
      @project = Project.find(params[:id])
    end

    def users_list
      @users = User.all.collect { |user| [user.full_name, user.id] }
    end

    def authorize_user
      if ManagerProject.where(project_id: params[:id], manager_id: current_manager.id).blank?
        redirect_to manager_projects_path, alert: 'You are not authorized for this action'
      end
    end
end
