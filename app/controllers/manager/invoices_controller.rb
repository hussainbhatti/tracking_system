class Manager::InvoicesController < ApplicationController
  before_action :set_project
  before_action :set_invoice, only: [:show, :edit, :destroy, :update]

  def index
    @invoices = @project.invoices
  end

  def new
    date_range = DateRange.new(invoice_params[:start_time], invoice_params[:end_time]) if params[:invoice].present?
    if params[:invoice].present? && date_range.valid_range?
      @tasks = @project.tasks.bill_able.filter_on date_range
      @invoice = @project.invoices.build(per_hour_rate: @project.hour_cost,
        total_hours: @project.total_hours_for(@tasks),
        start_time: invoice_params[:start_time],
        end_time: invoice_params[:end_time])
    else
      @invoice = @project.invoices.build
    end
  end

  def show
  end

  def edit
  end

  def create
    @invoice =  @project.invoices.build(invoice_params)
    if @invoice.save
      redirect_to manager_project_invoices_path(@project), notice: 'Invoice was created successfully.'
    else
      render :new
    end
  end

  def update
    if @invoice.update(invoice_params)
      redirect_to manager_project_invoices_path(@project), notice: 'Invoice was updated successfully.'
    else
      render :edit
    end
  end

  def destroy
    if @invoice.destroy
      redirect_to manager_project_invoices_path(@project)
    else
      redirect_to manager_project_invoices_path(@project), alert: 'Something went wrong'
    end
  end

  private
    def invoice_params
      params.require(:invoice).permit(:per_hour_rate, :total_hours, :start_time, :end_time, :project_id)
    end

    def set_project
      @project = Project.find(params[:project_id])
    end

    def set_invoice
      @invoice = @project.invoices.find(params[:id])
    end

end
