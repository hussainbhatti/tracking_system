module ApplicationHelper
  def flash_class_name(name)
    return 'success' if name == 'notice'
    return 'danger' if name == 'alert'
  end

  def date_formate date
    date.strftime("%m/%d/%Y")
  end

  def date_time_formate date
    date.strftime("%d/%m/%Y %I:%M %p")
  end
end
