module Manager::InvoicesHelper
  def total_amount per_hour_rate, total_hours
    per_hour_rate * total_hours if per_hour_rate.present? && total_hours.present?
  end
end
