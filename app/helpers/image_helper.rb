module ImageHelper
  def render_image_tag(image)
    image_tag(url_for(image)) if image.attached?
  end
end
