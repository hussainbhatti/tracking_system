Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :managers, controllers: { registrations: "managers/registrations", invitations: 'managers/invitations' }

  devise_scope :manager do
    post '/managers/invitation/:id/reinvite', to: 'managers/invitations#reinvite', as: :manager_invite
  end

  devise_for :users, controllers: { registrations: "users/registrations" }

  namespace :manager do
    resources :reports
    resources :expenses
    resources :projects do
      resources :tasks
      resources :invoices
    end
  end

  namespace :user do
    resources :projects do
      resources :tasks
    end
  end

  root "pages#index"
end
