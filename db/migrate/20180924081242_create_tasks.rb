class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.text :title
      t.text :discription
      t.belongs_to :taskable, polymorphic: true

      t.timestamps
    end
    add_index :tasks, [:taskable_id, :taskable_type]
  end
end
