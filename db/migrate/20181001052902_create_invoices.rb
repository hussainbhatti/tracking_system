class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.float :per_hour_rate
      t.float :total_hours
      t.float :total_amount

      t.timestamps
    end
  end
end
