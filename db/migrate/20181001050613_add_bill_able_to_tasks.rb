class AddBillAbleToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :bill_able, :boolean
  end
end
