class CreateExpenseReports < ActiveRecord::Migration[5.2]
  def change
    create_table :expense_reports do |t|
      t.references :report, foreign_key: true
      t.references :expense, foreign_key: true

      t.timestamps
    end
  end
end
