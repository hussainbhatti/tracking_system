class ChangeColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :firstname, :first_name
    rename_column :users, :lastname, :last_name
    rename_column :managers, :firstname, :first_name
    rename_column :managers, :lastname, :last_name
  end
end
