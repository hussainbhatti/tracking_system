class AddProjectToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_reference :invoices, :project, foreign_key: true
  end
end
