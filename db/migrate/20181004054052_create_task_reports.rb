class CreateTaskReports < ActiveRecord::Migration[5.2]
  def change
    create_table :task_reports do |t|
      t.references :report, foreign_key: true
      t.references :task, foreign_key: true

      t.timestamps
    end
  end
end
