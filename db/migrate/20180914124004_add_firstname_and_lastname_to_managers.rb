class AddFirstnameAndLastnameToManagers < ActiveRecord::Migration[5.2]
  def change
    add_column :managers, :firstname, :string
    add_column :managers, :lastname, :string
  end
end
