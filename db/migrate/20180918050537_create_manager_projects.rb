class CreateManagerProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :manager_projects do |t|
      t.references :manager, foreign_key: true
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
