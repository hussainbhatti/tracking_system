class AddHourCostToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :hour_cost, :float
  end
end
