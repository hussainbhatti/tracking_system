class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.float :total_income
      t.float :total_expense
      t.date :start_time
      t.date :end_time

      t.timestamps
    end
  end
end
