class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.boolean :recurring_expense
      t.string :title
      t.float :amount
      t.datetime :date
      t.integer :expense_type
      t.references :manager, foreign_key: true
      t.belongs_to :expenseable, polymorphic: true

      t.timestamps
    end
    add_index :expenses, [:expenseable_id, :expenseable_type]
  end
end
