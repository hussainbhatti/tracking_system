class AddStartTimeAndEndTimeToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :start_time, :datetime
    add_column :invoices, :end_time, :datetime
  end
end
